<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    protected $fillable = ['fullname', 'email', 'msisdn', 'gender', 'birthday', 'address', 'participantNo'];
}
