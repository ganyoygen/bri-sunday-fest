<?php

namespace App\Http\Controllers\Provider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterProviderController extends Controller
{
    public function run(Request $request)
    {
      echo json_encode($request->all());
    }
}
