<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Users;

class RegisterProviderController extends Controller
{
    public function sendMail($param)
    {
        $client = new Client([
            'base_uri' => 'https://platforms-202016.appspot.com/api/v1/',
            'form_params' => $param
        ]);

        $response = $client->request('POST', 'send');

        $code = $response->getStatusCode();

        return $code;

    }

    public function storeUser($param)
    {

        $client = new Client([
            'base_uri' => 'https://platforms-202016.appspot.com/api/v1/',
            'form_params' => $param
        ]);

        $response = $client->request('POST', 'collect');

        $code = $response->getStatusCode();

        if ($code !== 200) return response()->json([
            'error' => 400,
            'message' => 'Error store user data.'
        ]);

        return $code;
    }

    public function run(Request $request)
    {

        $validate = $request->validate([
            'fullname' => 'required',
            'email' => 'required',
            'msisdn' => 'required',
            'gender' => 'required',
            'birthday' => 'required',
            'address' => 'required'
        ]);

        $check = Users::where('email', $validate['email'])->first();

        if ($check) return response()->json([
            'error' => 400,
            'message' => 'This User Already Exists!'
        ], 400);

        $this->storeUser($validate);

        $resp = Users::create($validate);

        $sendMail = [
            'to' => $resp['email'],
            'from' => 'no-reply@brisundayfest.co.id',
            'subject' => 'Subject Here',
            'message' => 'Message Here'
        ];

        // Sementara saya comment karena akun di suspend.
        // $this->sendMail($sendMail);

        return response()->json($resp, 201);
    }
}
