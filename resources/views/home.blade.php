<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Laravel React application</title>
        <link href="{{mix('css/app.css')}}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css')}}">
        <script>
          window.EV = <?=json_encode([
            'BASE_URL' => getenv('BASE_URL')
            ])?>
          </script>
    </head>
    <body>
    <h2 style="text-align: center"> </h2>
        <div id="root"></div>
        <script src="{{mix('js/app.js')}}" ></script>
        <script src="{{ asset('bootstrap/js/bootstrap.min.js')}}"> </script>
    </body>
</html>
