import axios from 'axios';
import _ from 'lodash';
const APIUrl =  window.EV.BASE_URL;
const headers = { 'Content-Type': 'application/json' };
export const RegisterServices = {
  register
}

function register(data) {
    let request = axios.post(APIUrl+ '/api/registerProvider', data, { headers: headers })
    return (request.then(handleSuccess, handleError));
}


function handleError( response ) {
  if (! _.isObject( response.data ) || ! response.data.message) {
    return( Promise.reject( "An unknown error occurred." ) );
  }

  return( Promise.reject( response.data.message ) );
}

function handleSuccess( response ) {
  return( response.data );
}
