import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import Information from './Information';
import Registration from './Registration';

const Main = () => (
  <main>
    <Switch>
      <Route exact path="/" component={Home}/>
      <Route path="/informasi" component={Information}/>
      <Route path="/registrasi" component={Registration}/>
    </Switch>
  </main>
)

export default Main;
