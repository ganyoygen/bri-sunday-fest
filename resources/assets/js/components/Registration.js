import React, { Component } from 'react';
const APIUrl =  window.EV.BASE_URL;
import { RegisterServices } from '../services/RegisterServices';

export default class Registration extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const headers = { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    const APIUrl =  window.EV.BASE_URL;

    const data = {
      fullName: $("input[name=fullname]").val(),
      email: $("input[name=email]").val(),
      msisdn: $("input[name=msisdn]").val(),
      address: $("textarea[name=address]").val(),
      birthdate: $("input[name=birthdate]").val()
    }

    RegisterServices.register(data).then(response => {
      console.log(response);
    })

    // axios({
    //   method: 'post',
    //   url: APIUrl+ '/registerProvider',
    //   headers: headers,
    //   data: data
    //
    // })
    // .then( response => {
    //
    // })
    // .then( error => {
    //   console.log('Error');
    // });
  }

  render() {
    return (
      <div className="container">
        <h2>Registration component</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Full name</label>
            <input type="hidden" name="_hiddden"/>
            <input type="text" name="fullname" className="form-control" required/>
          </div>
          <div className="form-group">
            <label>Phone number</label>
            <input type="number" name="msisdn" className="form-control" required/>
          </div>
          <div className="form-group">
            <label>Email address</label>
            <input type="email" className="form-control" name="email" id="email" required/>
          </div>
          <div className="form-group">
            <label>Address</label>
            <textarea name="address" className="form-control" value="Address" required> </textarea>
          </div>
          <div className="form-group">
            <label>Birth date</label>
            <input type="date" name="birthdate" className="form-control" required/>
          </div>
          <button type="submit" className="btn btn-default">Submit</button>
        </form>
      </div>
    )
  }

}
